//
// Filename     : console_writer.go
// Description  : Writer for writing message to the console.
// Author       : Christophe Burki
// Maintainer   : Christophe Burki
// Version      : 1.0.0
//

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.

// -----------------------------------------------------------------------------

package logger

// -----------------------------------------------------------------------------

import (
	"os"
	"time"
)

type ConsoleWriter struct {
	logFormat      string
	datetimeFormat string
}

// -----------------------------------------------------------------------------

func NewConsoleWriter() *ConsoleWriter {

	cw := ConsoleWriter{
		defaultLogFormat,
		defaultDatetimeFormat,
	}

	return &cw
}

// -----------------------------------------------------------------------------

func (this *ConsoleWriter) write(msg string, name string, level int, date time.Time, location Location) {

	formatter := NewFormatter(this.logFormat, this.datetimeFormat)
	out := []byte(formatter.formatLog(msg, name, level, date, location) + "\n")
	os.Stdout.Write(out)
}

// -----------------------------------------------------------------------------

func (this *ConsoleWriter) setLogFormat(format string) {

	this.logFormat = format
}

// -----------------------------------------------------------------------------

func (this *ConsoleWriter) setDatetimeFormat(format string) {

	this.datetimeFormat = format
}

// -----------------------------------------------------------------------------
