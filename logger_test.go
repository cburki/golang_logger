// logger_test.go ---
//
// Filename     : logger_test.go
// Description  : Logger test suites
// Author       : Christophe Burki
// Maintainer   : Christophe Burki
// Version      : 1.0.0
//

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.

//
//
// -----------------------------------------------------------------------------

package logger

// -----------------------------------------------------------------------------

import (
	"bufio"
	"bytes"
	"os"
	"testing"
	"time"
)

// -----------------------------------------------------------------------------

// Test if the log output is what we expect to be.
//
// Write the log messages into a buffer a check if what we have written is what
// we expect to be.
func TestOutput(t *testing.T) {

	var b bytes.Buffer
	writer := NewBufferWriter(&b)
	writer.setDatetimeFormat("%Y-%m-%d")
	myLogger := NewLogger(DEBUG, "TestOutput", writer)
	now := time.Now().Format("2006-01-02")

	s := "EMERGENCY"
	myLogger.Emerg("This is an " + s + " message")
	want := now + " EMERGENCY [TestOutput] : This is an EMERGENCY message\n"
	if b.String() != want {
		t.Fatalf("Error : want \"%s\", got \"%s\"", want, b.String())
	}
	b.Reset()

	myLogger.Alert("This is an ALERT message")
	want = now + " ALERT [TestOutput] : This is an ALERT message\n"
	if b.String() != want {
		t.Fatalf("Error : want \"%s\", got \"%s\"", want, b.String())
	}
	b.Reset()

	myLogger.Crit("This is a CRITICAL message")
	want = now + " CRITICAL [TestOutput] : This is a CRITICAL message\n"
	if b.String() != want {
		t.Fatalf("Error : want \"%s\", got \"%s\"", want, b.String())
	}
	b.Reset()

	myLogger.Err("This is an ERROR message")
	want = now + " ERROR [TestOutput] : This is an ERROR message\n"
	if b.String() != want {
		t.Fatalf("Error : want \"%s\", got \"%s\"", want, b.String())
	}
	b.Reset()

	myLogger.Warning("This is a WARNING message")
	want = now + " WARNING [TestOutput] : This is a WARNING message\n"
	if b.String() != want {
		t.Fatalf("Error : want \"%s\", got \"%s\"", want, b.String())
	}
	b.Reset()

	myLogger.Notice("This is a NOTICE message")
	want = now + " NOTICE [TestOutput] : This is a NOTICE message\n"
	if b.String() != want {
		t.Fatalf("Error : want \"%s\", got \"%s\"", want, b.String())
	}
	b.Reset()

	myLogger.Info("This is an INFO message")
	want = now + " INFO [TestOutput] : This is an INFO message\n"
	if b.String() != want {
		t.Fatalf("Error : want \"%s\", got \"%s\"", want, b.String())
	}
	b.Reset()

	myLogger.Debug("This is a DEBUG message")
	want = now + " DEBUG [TestOutput] : This is a DEBUG message\n"
	if b.String() != want {
		t.Fatalf("Error : want \"%s\", got \"%s\"", want, b.String())
	}
	b.Reset()
}

// -----------------------------------------------------------------------------

// Test if the level are correctly handled.
//
// Write the log message into a buffer. Check the buffer length in order
// to see if the message was written or not. The buffer for the INFO and DEBUG
// message must be of length 0 because the level is set to NOTICE.
func TestLevel(t *testing.T) {

	var b bytes.Buffer
	writer := NewBufferWriter(&b)
	myLogger := NewLogger(NOTICE, "TestLevel", writer)

	s := "EMERGENCY"
	myLogger.Emerg("This is an " + s + " message")
	if b.Len() == 0 {
		t.Fatal("EMERGENCY message must be written")
	}
	b.Reset()

	myLogger.Alert("This is an ALERT message")
	if b.Len() == 0 {
		t.Fatal("ALERT message must be written")
	}
	b.Reset()

	myLogger.Crit("This is a CRITICAL message")
	if b.Len() == 0 {
		t.Fatal("CRITICAL message must be written")
	}
	b.Reset()

	myLogger.Err("This is an ERROR message")
	if b.Len() == 0 {
		t.Fatal("ERROR message must be written")
	}
	b.Reset()

	myLogger.Warning("This is a WARNING message")
	if b.Len() == 0 {
		t.Fatal("WARNING message must be written")
	}
	b.Reset()

	myLogger.Notice("This is a NOTICE message")
	if b.Len() == 0 {
		t.Fatal("NOTICE message must be written")
	}
	b.Reset()

	myLogger.Info("This is an INFO message")
	if b.Len() > 0 {
		t.Fatal("INFO message must not be written")
	}
	b.Reset()

	myLogger.Debug("This is a DEBUG message")
	if b.Len() > 0 {
		t.Fatal("DEBUG message must not be writen")
	}
	b.Reset()
}

// -----------------------------------------------------------------------------

// Test writing log messages to standard output.
//
// Debug and info message must not be shown because the level is set to NOTICE.
func TestConsoleWriter(t *testing.T) {

	writer := NewConsoleWriter()
	myLogger := NewLogger(NOTICE, "TestConsoleWriter", writer)

	s := "EMERGENCY"
	myLogger.Emerg("This is an " + s + " message")

	s = "ALERT"
	myLogger.Alert("This is an " + s + " message")

	s = "CRITICAL"
	myLogger.Crit("This is an " + s + " message")

	s = "ERROR"
	myLogger.Err("This is an " + s + " message")

	s = "WARNING"
	myLogger.Warning("This is an " + s + " message")

	s = "NOTICE"
	myLogger.Notice("This is an " + s + " message")

	s = "INFO"
	myLogger.Info("This is an " + s + " message")

	s = "DEBUG"
	myLogger.Debug("This is an " + s + " message")
}

// -----------------------------------------------------------------------------

// Test writing log messages to a file.
//
// Compare the lines written and what we expect to be written.
func TestFileWriter(t *testing.T) {

	filename := "test.log"
	writer := NewFileWriter(filename, false)
	writer.setDatetimeFormat("%Y-%m-%d")
	myLogger := NewLogger(DEBUG, "TestFileWriter", writer)

	s := "EMERGENCY"
	myLogger.Emerg("This is an " + s + " message")
	myLogger.Alert("This is an ALERT message")
	myLogger.Crit("This is a CRITICAL message")
	myLogger.Err("This is an ERROR message")
	myLogger.Warning("This is a WARNING message")
	myLogger.Notice("This is a NOTICE message")
	myLogger.Info("This is an INFO message")
	myLogger.Debug("This is a DEBUG message")

	now := time.Now().Format("2006-01-02")
	wants := []string{
		now + " EMERGENCY [TestFileWriter] : This is an EMERGENCY message",
		now + " ALERT [TestFileWriter] : This is an ALERT message",
		now + " CRITICAL [TestFileWriter] : This is a CRITICAL message",
		now + " ERROR [TestFileWriter] : This is an ERROR message",
		now + " WARNING [TestFileWriter] : This is a WARNING message",
		now + " NOTICE [TestFileWriter] : This is a NOTICE message",
		now + " INFO [TestFileWriter] : This is an INFO message",
		now + " DEBUG [TestFileWriter] : This is a DEBUG message",
	}

	file, err := os.Open(filename)
	if err != nil {
		t.Fatalf("Failed to open log file for reading : %s\n", err)
	}
	defer file.Close()

	i := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {

		line := scanner.Text()
		if line != wants[i] {

			t.Fatalf("Error : want \"%s\", got \"%s\"", wants[i], line)
		}
		i++
	}
}

// -----------------------------------------------------------------------------

// Test writing log message to both a file and standard output.
//
// Debug message must not be shown because the level is set to INFO.
func _TestMultiWriter(t *testing.T) {

	cWriter := NewConsoleWriter()
	fWriter := NewFileWriter("test_multi.log", false)
	writers := map[string]Writer{
		"CONSOLE": cWriter,
		"FILE":    fWriter,
	}
	writer := NewMultiWriter(writers)
	myLogger := NewLogger(INFO, "TestMultiWriter", writer)

	s := "EMERGENCY"
	myLogger.Emerg("This is an " + s + " message")
	myLogger.Alert("This is an ALERT message")
	myLogger.Crit("This is a CRITICAL message")
	myLogger.Err("This is an ERROR message")
	myLogger.Warning("This is a WARNING message")
	myLogger.Notice("This is a NOTICE message")
	myLogger.Info("This is an INFO message")
	myLogger.Debug("This is a DEBUG message")
}

// -----------------------------------------------------------------------------

// Test formatter
//
//
func TestFormatter(t *testing.T) {

	location := Location{
		"Filename",
		"Funcname",
		0,
	}

	// Test message format
	formatter := NewFormatter("%l [%n] %f/%c %b : %m", defaultDatetimeFormat)
	message := formatter.formatLog("MESSAGE", "NAME", INFO, time.Now(), location)
	want := "INFO [NAME] Filename/Funcname 0 : MESSAGE"
	if message != want {
		t.Fatalf("Error : want \"%s\", got \"%s\"", want, message)
	}

	// Test datatime format
	refTime, _ := time.Parse(time.UnixDate, "Sat Jan 16 18:24:39 CST 2016")
	want = "2016-01-16 18:24:39"
	datetime := formatter.formatDatetime(refTime)
	if datetime != want {
		t.Fatalf("Error : want \"%s\", got \"%s\"", want, datetime)
	}
}

// -----------------------------------------------------------------------------

// Test property configurator
//
//
func TestPropertyConfigurator(t *testing.T) {

	// Test missing required properties
	propsMissing := map[string]string{
		"logger.writer.CONSOLE": "ConsoleWriter",
	}
	ConfigureFromMap(propsMissing)

	// Test property configuration from map
	props := map[string]string{
		"logger.rootLogger.level":      "INFO",
		"logger.rootLogger.writers":    "CONSOLE, FILE",
		"logger.writer.CONSOLE":        "ConsoleWriter",
		"logger.writer.CONSOLE.format": "%d %l [%n] %f %b : %m",
		"logger.writer.FILE":           "FileWriter",
		"logger.writer.FILE.filename":  "test_factory.log",
		"logger.writer.FILE.append":    "false",
		"logger.writer.FILE.format":    "%d %l [%n] : %m",
		"logger.writer.FILE.dtformat":  "%Y-%m-%d",
		"logger.level.Test_1":          "DEBUG",
		"logger.level.Test_2":          "WARNING",
	}
	ConfigureFromMap(props)

	want := "{ \"logger.level.Test_1\" : \"DEBUG\", \"logger.level.Test_2\" : \"WARNING\", \"logger.rootLogger.level\" : \"INFO\", \"logger.rootLogger.writers\" : \"CONSOLE, FILE\", \"logger.writer.CONSOLE\" : \"ConsoleWriter\", \"logger.writer.CONSOLE.format\" : \"%d %l [%n] %f %b : %m\", \"logger.writer.FILE\" : \"FileWriter\", \"logger.writer.FILE.append\" : \"false\", \"logger.writer.FILE.dtformat\" : \"%Y-%m-%d\", \"logger.writer.FILE.filename\" : \"test_factory.log\", \"logger.writer.FILE.format\" : \"%d %l [%n] : %m\" }"
	got := Properties2String()
	if want != got {
		t.Fatalf("Error : want \"%s\", got \"%s\"", want, got)
	}

	// Test property configuration from file
	ConfigureFromFile("test.properties")
	got = Properties2String()
	if want != got {
		t.Fatalf("Error : want \"%s\", got \"%s\"", want, got)
	}
}

// -----------------------------------------------------------------------------

// Test property configurator
//
//
func TestLoggerFactory(t *testing.T) {

	var err error

	// Test property not configured
	ClearProperties()
	//_, err = GetLogger("TestNotConfigured")
	//if err == nil {
	//	t.Fatalf("Error : must failed when not configured")
	//}

	// Test no writers defined
	propsNoWriter := map[string]string{
		"logger.rootLogger.level":   "INFO",
		"logger.rootLogger.writers": "CONSOLE",
		"logger.level.Test_1":       "DEBUG",
		"logger.level.Test_2":       "WARNING",
	}
	ConfigureFromMap(propsNoWriter)
	_, err = GetLogger("TestNoWriter")
	if err == nil {
		t.Fatalf("Error : must failed when no writer configured")
	}

	// Test unknown writer
	propsUnkownWriter := map[string]string{
		"logger.rootLogger.level":   "INFO",
		"logger.rootLogger.writers": "UNKNOWN",
		"logger.writer.CONSOLE":     "UnknownWriter",
		"logger.level.Test_1":       "DEBUG",
		"logger.level.Test_2":       "WARNING",
	}
	ConfigureFromMap(propsUnkownWriter)
	_, err = GetLogger("TestUnknownWriter")
	if err == nil {
		t.Fatalf("Error : must failed when unknwon writer configured")
	}

	// Test bad properties for file writer
	propsBadFile := map[string]string{
		"logger.rootLogger.level":   "INFO",
		"logger.rootLogger.writers": "FILE",
		"logger.writer.FILE":        "FileWriter",
		"logger.writer.FILE.append": "false",
		"logger.level.Test_1":       "DEBUG",
		"logger.level.Test_2":       "WARNING",
	}
	ConfigureFromMap(propsBadFile)
	_, err = GetLogger("TestBadFile")
	if err == nil {
		t.Fatalf("Error : must failed when missing file writer option")
	}

	// Test using buffer writer
	propsBufferWriter := map[string]string{
		"logger.rootLogger.level":   "INFO",
		"logger.rootLogger.writers": "BUFFER",
		"logger.writer.BUFFER":      "BufferWriter",
		"logger.level.Test_1":       "DEBUG",
		"logger.level.Test_2":       "WARNING",
	}
	ConfigureFromMap(propsBufferWriter)
	_, err = GetLogger("TestBufferWriter")
	if err == nil {
		t.Fatalf("Error : must failed when using buffer writer")
	}

	// Test using multi writer
	propsMultiWriter := map[string]string{
		"logger.rootLogger.level":   "INFO",
		"logger.rootLogger.writers": "MULTI",
		"logger.writer.MULTI":       "MultiWriter",
		"logger.level.Test_1":       "DEBUG",
		"logger.level.Test_2":       "WARNING",
	}
	ConfigureFromMap(propsMultiWriter)
	_, err = GetLogger("TestMultiWriter")
	if err == nil {
		t.Fatalf("Error : must failed when using multi writer")
	}

	// Test logging
	props := map[string]string{
		"logger.rootLogger.level":      "INFO",
		"logger.rootLogger.writers":    "CONSOLE, FILE",
		"logger.writer.CONSOLE":        "ConsoleWriter",
		"logger.writer.CONSOLE.format": "%d %l [%n] %f %b : %m",
		"logger.writer.FILE":           "FileWriter",
		"logger.writer.FILE.filename":  "test_factory.log",
		"logger.writer.FILE.append":    "false",
		"logger.writer.FILE.format":    "%d %l [%n] : %m",
		"logger.writer.FILE.dtformat":  "%Y-%m-%d",
		"logger.level.Test_1":          "DEBUG",
		"logger.level.Test_2":          "WARNING",
	}
	ConfigureFromMap(props)

	logger1, err := GetLogger("Test_1")
	if err != nil {
		t.Fatalf("Error : unable to get logger : %s\n", err)
	}

	logger2, err := GetLogger("Test_2")
	if err != nil {
		t.Fatalf("Error : unable to get logger : %s\n", err)
	}

	s := "EMERGENCY"
	logger1.Emerg("This is an " + s + " message")
	logger1.Alert("This is an ALERT message")
	logger1.Crit("This is a CRITICAL message")
	logger1.Err("This is an ERROR message")
	logger1.Warning("This is a WARNING message")
	logger1.Notice("This is a NOTICE message")
	logger1.Info("This is an INFO message")
	logger1.Debug("This is a DEBUG message")

	logger2.Emerg("This is an " + s + " message")
	logger2.Alert("This is an ALERT message")
	logger2.Crit("This is a CRITICAL message")
	logger2.Err("This is an ERROR message")
	logger2.Warning("This is a WARNING message")
	logger2.Notice("This is a NOTICE message")
	logger2.Info("This is an INFO message")
	logger2.Debug("This is a DEBUG message")

	// Get a logger a second time, we must have the same logger
	logger1b, err := GetLogger("Test_1")
	if logger1 != logger1b {
		t.Fatalf("Error : this is not the same logger, first : %v, second : %v", logger1, logger1b)
	}
}

// -----------------------------------------------------------------------------

// Test the configuration of the logging system using code.
//
//
func TestSetupFromCode(t *testing.T) {

	var err error

	ClearProperties()
	logger, err := GetLogger("Test")
	if err != nil {
		t.Fatalf("Error : unable to get logger : %s\n", err)
	}
	cWriter := NewConsoleWriter()
	fWriter := NewFileWriter("test_setup.log", true)
	err = logger.AddWriter("CONSOLE", cWriter)
	if err != nil {
		t.Fatalf("Error : unable to add writer : %s\n", err)
	}
	err = logger.AddWriter("FILE", fWriter)
	if err != nil {
		t.Fatalf("Error : unable to add writer : %s\n", err)
	}
	logger.SetLevel(NOTICE)
	w, err := logger.GetWriter("CONSOLE")
	if err != nil {
		t.Fatalf("Error : unable to get writer : %s\n", err)
	}
	w.setLogFormat("%d %l [%n] %f/%c (%b) : %m")

	s := "EMERGENCY"
	logger.Emerg("This is an " + s + " message")
	logger.Alert("This is an ALERT message")
	logger.Crit("This is a CRITICAL message")
	logger.Err("This is an ERROR message")
	logger.Warning("This is a WARNING message")
	logger.Notice("This is a NOTICE message")
	logger.Info("This is an INFO message")
	logger.Debug("This is a DEBUG message")

	err = logger.RemoveWriter("CONSOLE")
	if err != nil {
		t.Fatalf("Error : unable to remove writer : %s\n", err)
	}
	logger.Emerg("This is an " + s + " message")
	logger.Alert("This is an ALERT message")
	logger.Crit("This is a CRITICAL message")
	logger.Err("This is an ERROR message")
	logger.Warning("This is a WARNING message")
	logger.Notice("This is a NOTICE message")
	logger.Info("This is an INFO message")
	logger.Debug("This is a DEBUG message")
}

// -----------------------------------------------------------------------------
