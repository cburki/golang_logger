//
// Filename     : logger.go
// Description  : Logger package.
// Author       : Christophe Burki
// Maintainer   : Christophe Burki
// Version      : 1.0.0
//

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.

// -----------------------------------------------------------------------------

package logger

// -----------------------------------------------------------------------------

import (
	"errors"
	"fmt"
	"path"
	"reflect"
	"runtime"
	"strings"
	"time"
)

type Logger struct {
	level  int
	name   string
	writer Writer
}

// -----------------------------------------------------------------------------

//
func NewLogger(level int, name string, writer Writer) *Logger {

	l := Logger{
		level,
		name,
		writer,
	}
	return &l
}

// -----------------------------------------------------------------------------

func (this *Logger) write(level int, location Location, msg string) {

	this.writer.write(msg, this.name, level, time.Now(), location)
}

// -----------------------------------------------------------------------------

func (this *Logger) Log(level int, msg string) {

	if this.level <= level {

		_, filename, line, ok := runtime.Caller(2)
		if !ok {

			filename = "???"
			line = 0
		}

		location := Location{
			path.Base(filename),
			"",
			line,
		}
		this.write(level, location, msg)
	}
}

// -----------------------------------------------------------------------------

func (this *Logger) Emerg(msg string) {

	this.Log(EMERG, msg)
}

// -----------------------------------------------------------------------------

func (this *Logger) Alert(msg string) {

	this.Log(ALERT, msg)
}

// -----------------------------------------------------------------------------

func (this *Logger) Crit(msg string) {

	this.Log(CRIT, msg)
}

// -----------------------------------------------------------------------------

func (this *Logger) Err(msg string) {

	this.Log(ERR, msg)
}

// -----------------------------------------------------------------------------

func (this *Logger) Warning(msg string) {

	this.Log(WARNING, msg)
}

// -----------------------------------------------------------------------------

func (this *Logger) Notice(msg string) {

	this.Log(NOTICE, msg)
}

// -----------------------------------------------------------------------------

func (this *Logger) Info(msg string) {

	this.Log(INFO, msg)
}

// -----------------------------------------------------------------------------

func (this *Logger) Debug(msg string) {

	this.Log(DEBUG, msg)
}

// -----------------------------------------------------------------------------

func (this *Logger) SetLevel(level int) {

	this.level = level
}

// -----------------------------------------------------------------------------

func (this *Logger) GetLevel() int {

	return this.level
}

// -----------------------------------------------------------------------------

func (this *Logger) SetWriter(writer Writer) {

	this.writer = writer
}

// -----------------------------------------------------------------------------

func (this *Logger) AddWriter(name string, writer Writer) error {

	if strings.Compare(reflect.TypeOf(this.writer).String(), "*logger.MultiWriter") != 0 {

		return errors.New("Could not add a writer to a non multi writer")
	}

	if _, ok := this.writer.(*MultiWriter).getWriters()[name]; ok {

		fmt.Printf("WARNING : Writer with that name \"%s\" already exist, override it", name)
	}

	this.writer.(*MultiWriter).addWriter(name, writer)
	return nil
}

// -----------------------------------------------------------------------------

func (this *Logger) GetWriter(name string) (Writer, error) {

	if name == "" {

		return this.writer, nil
	}

	if strings.Compare(reflect.TypeOf(this.writer).String(), "*logger.MultiWriter") != 0 {

		return nil, errors.New("Could not get a writer with name from a non multi writer")
	}

	if _, ok := this.writer.(*MultiWriter).getWriters()[name]; !ok {

		fmt.Printf("WARNING : No writer exist with the name \"%s\"", name)
		return nil, nil
	}

	return this.writer.(*MultiWriter).getWriters()[name], nil
}

// -----------------------------------------------------------------------------

func (this *Logger) RemoveWriter(name string) error {

	if strings.Compare(reflect.TypeOf(this.writer).String(), "*logger.MultiWriter") != 0 {

		return errors.New("Could not remove a writer with name from a non multi writer")
	}

	delete(this.writer.(*MultiWriter).getWriters(), name)
	return nil
}

// -----------------------------------------------------------------------------
