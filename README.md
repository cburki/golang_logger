[![build status](https://gitlab.com/cburki/golang_logger/badges/master/build.svg)](https://gitlab.com/cburki/golang_logger/commits/master)

---

Summary
-------

Configurable logging facility library for python. Output of messages are handled
by *writers* which defines where the messages are written. A message could be
written to several writers simultaneously each with their own output formats.
A logger can be created for each components of your system and defines different
levels for each components. Debug messages can also be easily enabled or disabled
for each components.

Here is a configuration properties sample with tow writers *CONSOLE* and *FILE*.
Log levels is declared for two components *Test_1* and *Test_2*.

    logger.rootLogger.level      = INFO
    logger.rootLogger.writers    = CONSOLE, FILE
    logger.writer.CONSOLE        = ConsoleWriter
    logger.writer.CONSOLE.format = %d %l [%n] %f/%c %b : %m
    logger.writer.FILE           = FileWriter
    logger.writer.FILE.filename  = test_factory.log
    logger.writer.FILE.append    = false
    logger.writer.FILE.format    = %d %l [%n] : %m
    logger.writer.FILE.dtformat  = %Y-%m-%d
    logger.level.Test_1          = DEBUG
    logger.level.Test_2          = WARNING

The sample below show how to configure the logging system using the properties
file above and how to create a logger for the component *Test_2*.

    import (
        "gitlab.burkionline.net/cburki/logger"
    )

    logger.ConfigureFromFile("property_file.ini")
    myLogger := logger.GetLogger("Test_2")
    myLogger.Err("This is an error message")
    myLogger.Debug("This is a debug message")

The *debug* message is not written because for the *Test_2* logger the level
is set to *WARNING*.

More information about this library and how to use it can be found on the project web
site [https://golang-logger.burkionline.net](https://golang-logger.burkionline.net).


Installation
------------

The logging library could simply be installed using go get.

    go get gitlab.com/cburki/golang_logger
