//
// Filename     : console_writer.go
// Description  : Writer for writing message to several writers simultaneously.
// Author       : Christophe Burki
// Maintainer   : Christophe Burki
// Version      : 1.0.0
//

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.

// -----------------------------------------------------------------------------

package logger

// -----------------------------------------------------------------------------

import (
	"time"
)

type MultiWriter struct {
	writers        map[string]Writer
	logFormat      string
	datetimeFormat string
}

// -----------------------------------------------------------------------------

func NewMultiWriter(writers map[string]Writer) *MultiWriter {

	mw := MultiWriter{
		writers,
		defaultLogFormat,
		defaultDatetimeFormat,
	}

	return &mw
}

// -----------------------------------------------------------------------------

func (this *MultiWriter) write(msg string, name string, level int, date time.Time, location Location) {

	for _, writer := range this.writers {

		writer.write(msg, name, level, date, location)
	}
}

// -----------------------------------------------------------------------------

func (this *MultiWriter) setLogFormat(format string) {

	this.logFormat = format
}

// -----------------------------------------------------------------------------

func (this *MultiWriter) setDatetimeFormat(format string) {

	this.datetimeFormat = format
}

// -----------------------------------------------------------------------------

func (this *MultiWriter) getWriters() map[string]Writer {

	return this.writers
}

// -----------------------------------------------------------------------------

func (this *MultiWriter) addWriter(name string, writer Writer) {

	this.writers[name] = writer
}

// -----------------------------------------------------------------------------
