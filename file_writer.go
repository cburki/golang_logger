//
// Filename     : file_writer.go
// Description  : Writer for writing message into a file.
// Author       : Christophe Burki
// Maintainer   : Christophe Burki
// Version      : 1.0.0
//

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.

// -----------------------------------------------------------------------------

package logger

// -----------------------------------------------------------------------------

import (
	"fmt"
	"os"
	"time"
)

type FileWriter struct {
	filename       string
	mode           int
	file           *os.File
	logFormat      string
	datetimeFormat string
}

// -----------------------------------------------------------------------------

func NewFileWriter(filename string, append bool) *FileWriter {

	mode := os.O_RDWR | os.O_CREATE
	if append {
		mode |= os.O_APPEND
	}

	file, err := os.OpenFile(filename, mode, 0666)
	if err != nil {

		fmt.Printf("Error while opening file \"%s\" : %s\n", filename, err)
		return nil
	}

	fw := FileWriter{
		filename,
		mode,
		file,
		defaultLogFormat,
		defaultDatetimeFormat,
	}

	return &fw
}

// -----------------------------------------------------------------------------

func DestroyFileWriter(fileWriter *FileWriter) {

	if fileWriter != nil {

		fileWriter.file.Close()
	}
}

// -----------------------------------------------------------------------------

func (this *FileWriter) write(msg string, name string, level int, date time.Time, location Location) {

	formatter := NewFormatter(this.logFormat, this.datetimeFormat)
	out := []byte(formatter.formatLog(msg, name, level, date, location) + "\n")
	_, err := this.file.Write(out)
	if err != nil {

		fmt.Printf("Errot while writing to file \"%s\" : %s\n", this.filename, err)
	}
}

// -----------------------------------------------------------------------------

func (this *FileWriter) setLogFormat(format string) {

	this.logFormat = format
}

// -----------------------------------------------------------------------------

func (this *FileWriter) setDatetimeFormat(format string) {

	this.datetimeFormat = format
}

// -----------------------------------------------------------------------------
