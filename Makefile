### Makefile --- 
## 
## Filename     : Makefile
## Description  : 
## Author       : Christophe Burki
## Maintainer   : Christophe Burki
## Created      : Tue Oct 27 14:07:17 2015
## Version      : 1.0.0
## Last-Updated : Tue Jan 19 20:43:50 2016 (3600 CET)
##           By : Christophe Burki
##     Update # : 10
## 
######################################################################
## 
### Commentary   : 
## 
## 
######################################################################
## 
### Change log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License version 3 as
## published by the Free Software Foundation.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file LICENSE.  If not, write to the
## Free Software Foundation, Inc., 51 Franklin Street, Fifth
## ;; Floor, Boston, MA 02110-1301, USA.
## 
######################################################################

GOLIBS := /opt/$(shell whoami)/go_libs
PACKAGE := logger
PKGPATH := gitlab.burkionline.net/cburki
PKGSRCPATH := $(GOLIBS)/src/$(PKGPATH)/$(PACKAGE)
GOPATH := $(GOLIBS):$(shell pwd)
TEST_SOURCES := $(wildcard *_test.go)
SOURCES := $(filter-out $(TEST_SOURCES), $(wildcard *.go))


#
# --------------------------------------------------------------------
#

.PHONY : all clean distclean

all : $(PACKAGE)


$(PACKAGE) : $(SOURCES)
	mkdir -p $(PKGSRCPATH)
	rm -f $(PKGSRCPATH)/*.go
	cp $(SOURCES) $(PKGSRCPATH)/.
	GOPATH=$(GOPATH) go install $(PKGPATH)/$(PACKAGE)

test :
	GOPATH=$(GOPATH) go test

clean :
	rm -Rf *.o $(TARGET)

distclean : clean
	rm -f *~
	rm -f *.log

vet :
	go vet $(SOURCES)
	go vet $(TEST_SOURCES)

fmt :
	gofmt -d=true -w=true $(SOURCES)
	gofmt -d=true -w=true $(TEST_SOURCES)

get :
	mkdir -p $(GOLIBS)
	cd $(GOLIBS)
	GOPATH=$(GOPATH) go get -v -u github.com/go-ini/ini
