//
// Filename     : logger_factory.go
// Description  : Logger factory for instancianting new loggers.
// Author       : Christophe Burki
// Maintainer   : Christophe Burki
// Version      : 1.0.0
//

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.

// -----------------------------------------------------------------------------

package logger

// -----------------------------------------------------------------------------

import (
	"errors"
	"fmt"
	"strconv"
)

var loggers map[string]*Logger

// -----------------------------------------------------------------------------

func init() {

	loggers = make(map[string]*Logger)
}

// -----------------------------------------------------------------------------

func GetLogger(name string) (*Logger, error) {

	// Check if the logging system has been configured.
	if !property.isConfigured() {

		fmt.Printf("WARNING : Logger is not configured\n")
		//return nil, errors.New("Logger is not configured")
	}

	// Return the logger if it has already been created.
	if logger, ok := loggers[name]; ok {

		return logger, nil
	}

	// Find the logger level.
	loggerLogLevel := property.getLoggerLevel(name)

	// Find all writers from property logger.rootLogger.writers and instantiate them.
	rootWriters, error := property.getRootWriters()
	if error != nil {

		return nil, error
	}

	var writers map[string]Writer
	writers = make(map[string]Writer)

	for _, writerName := range rootWriters {

		// Find the writer for this name
		if _, ok := property.getProperties()["logger.writer."+writerName]; !ok {

			return nil, errors.New("No writer found for \"" + writerName + "\"")
		}

		// Retrieve the format and datetime format
		format := property.getProperty("logger.writer."+writerName+".format", defaultLogFormat)
		datetimeFormat := property.getProperty("logger.writer."+writerName+".dtformat", defaultDatetimeFormat)

		// Instanciate the writers
		writerStructName := property.getProperty("logger.writer."+writerName, "Unknown")
		switch writerStructName {
		case "ConsoleWriter":

			consoleWriter := NewConsoleWriter()
			consoleWriter.setLogFormat(format)
			consoleWriter.setDatetimeFormat(datetimeFormat)
			writers[writerName] = consoleWriter

		case "FileWriter":

			filename := property.getProperty("logger.writer."+writerName+".filename", "")
			if filename == "" {

				return nil, errors.New("Missing filename option for FileWriter")
			}

			appendMode, err := strconv.ParseBool(property.getProperty("logger.writer."+writerName+".append", "true"))
			if err != nil {

				return nil, errors.New("Bad value for append option for FileWriter")
			}

			fileWriter := NewFileWriter(filename, appendMode)
			fileWriter.setLogFormat(format)
			fileWriter.setDatetimeFormat(datetimeFormat)
			writers[writerName] = fileWriter

		case "BufferWriter":

			return nil, errors.New("BufferWriter could not be instanciated from the logger factory")

		case "MultiWriter":

			return nil, errors.New("MultiWriter could not be instanciated from the logger factory")

		default:

			return nil, errors.New("Unknown writer \"" + writerName + "\"")
		}
	}

	writer := NewMultiWriter(writers)
	logger := NewLogger(loggerLogLevel, name, writer)
	loggers[name] = logger

	return logger, nil
}

// -----------------------------------------------------------------------------
