//
// Filename     : level.go
// Description  : Defines the log levels and facilities.
// Author       : Christophe Burki
// Maintainer   : Christophe Burki
// Version      : 1.0.0
//

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.

// -----------------------------------------------------------------------------

package logger

// -----------------------------------------------------------------------------

const (
	EMERG   int = 7
	ALERT   int = 6
	CRIT    int = 5
	ERR     int = 4
	WARNING int = 3
	NOTICE  int = 2
	INFO    int = 1
	DEBUG   int = 0
)

var string2LogLevel = map[string]int{
	"EMERG":   EMERG,
	"ALERT":   ALERT,
	"CRIT":    CRIT,
	"ERR":     ERR,
	"WARNING": WARNING,
	"NOTICE":  NOTICE,
	"INFO":    INFO,
	"DEBUG":   DEBUG,
}

var logLevel2String = map[int]string{
	EMERG:   "EMERGENCY",
	ALERT:   "ALERT",
	CRIT:    "CRITICAL",
	ERR:     "ERROR",
	WARNING: "WARNING",
	NOTICE:  "NOTICE",
	INFO:    "INFO",
	DEBUG:   "DEBUG",
}

// -----------------------------------------------------------------------------
