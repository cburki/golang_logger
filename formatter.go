//
// Filename     : formatter.go
// Description  :
// Author       : Christophe Burki
// Maintainer   : Christophe Burki
// Version      : 1.0.0
//

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.

// -----------------------------------------------------------------------------

package logger

// -----------------------------------------------------------------------------

import (
	"strconv"
	"strings"
	"time"
)

type Formatter struct {
	logFormat      string
	datetimeFormat string
}

const (
	defaultLogFormat      = "%d %l [%n] : %m"
	defaultDatetimeFormat = "%Y-%m-%d %H:%M:%S"
)

// -----------------------------------------------------------------------------

func NewFormatter(logFormat string, datetimeFormat string) *Formatter {

	f := Formatter{
		logFormat,
		datetimeFormat,
	}
	return &f
}

// -----------------------------------------------------------------------------

// Format a log message according to the format.
func (this *Formatter) formatLog(msg string, name string, level int, date time.Time, location Location) string {

	message := this.logFormat
	replacement := map[string]string{
		"%d": this.formatDatetime(date),   // Date and time
		"%l": logLevel2String[level],      // Log level
		"%n": name,                        // Logger name
		"%m": msg,                         // Message
		"%f": location.Filename,           // Filename
		"%b": strconv.Itoa(location.Line), // Line number
		"%c": location.Funcname,           // Function name
	}

	for k, v := range replacement {

		message = strings.Replace(message, k, v, 1)
	}

	return message
}

// -----------------------------------------------------------------------------

// Format the date according to the datetime format.
//
//
func (this *Formatter) formatDatetime(datetime time.Time) string {

	goFormat := this.datetimeFormat
	replacement := map[string]string{
		"%Y": "2006",  // Year (4 digits)
		"%m": "01",    // Month as a decimal number (2 digits)
		"%d": "02",    // Day of the month as a decimal number (2 digits)
		"%e": "2",     // Day of the month as a decimal number (1 digit)
		"%H": "15",    // Hour in 24 h format
		"%I": "3PM",   // Hour in 12 h format
		"%M": "04",    // Minutes
		"%S": "05",    // Seconds
		"%a": "Mon",   // Abbreviated week day
		"%b": "Jan",   // Abbreviated month name
		"%Z": "MST",   // Timezone abbreviation
		"%z": "-0700", // Offset from UTC timezone
	}

	for k, v := range replacement {

		goFormat = strings.Replace(goFormat, k, v, 1)
	}

	return datetime.Format(goFormat)
}

// -----------------------------------------------------------------------------
