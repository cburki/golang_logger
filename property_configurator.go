//
// Filename     : property_configurator.go
// Description  : Logging properties configuration.
// Author       : Christophe Burki
// Maintainer   : Christophe Burki
// Version      : 1.0.0
//

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.

// -----------------------------------------------------------------------------

package logger

// -----------------------------------------------------------------------------

import (
	"errors"
	"sort"
	"strings"

	"github.com/go-ini/ini"
)

type Property struct {
	Properties map[string]string
}

var property Property

// -----------------------------------------------------------------------------

func ConfigureFromMap(properties map[string]string) error {

	property = Property{
		make(map[string]string),
	}

	for k, v := range properties {
		property.Properties[k] = v
	}

	return nil
}

// -----------------------------------------------------------------------------

func ConfigureFromFile(filename string) error {

	property = Property{
		make(map[string]string),
	}

	config, err := ini.Load(filename)
	if err != nil {

		return errors.New("Error while loading property from file \"" + filename + "\" : " + err.Error())
	}

	return ConfigureFromMap(config.Section("").KeysHash())
}

// -----------------------------------------------------------------------------

func ClearProperties() {

	property = Property{
		make(map[string]string),
	}
}

// -----------------------------------------------------------------------------

// Return a string representation of the properties. The keys are alphabetically
// ordered.
func Properties2String() string {

	var keys []string
	for k := range property.Properties {

		keys = append(keys, k)
	}
	sort.Strings(keys)

	out := "{ "
	for _, k := range keys {

		out += "\"" + k + "\" : \"" + property.Properties[k] + "\""
		if k != keys[len(keys)-1] {
			out += ", "
		}
	}
	out += " }"

	return out
}

// -----------------------------------------------------------------------------

func (this *Property) isConfigured() bool {

	return len(this.Properties) != 0
}

// -----------------------------------------------------------------------------

func (this *Property) getProperties() map[string]string {

	return this.Properties
}

// -----------------------------------------------------------------------------

func (this *Property) getProperty(key string, defValue string) string {

	if value, ok := this.Properties[key]; ok {

		return value
	}

	return defValue
}

// -----------------------------------------------------------------------------

func (this *Property) getRootWriters() ([]string, error) {

	var values string
	var ok bool
	writers := make([]string, 0)

	if values, ok = this.Properties["logger.rootLogger.writers"]; !ok {

		return writers, nil
	}

	writers = strings.Split(values, ",")
	// Trim all string in place
	for i, writer := range writers {

		writers[i] = strings.Trim(writer, " ")
	}

	return writers, nil
}

// -----------------------------------------------------------------------------

func (this *Property) getRootLevel() int {

	if level, ok := this.Properties["logger.rootLogger.level"]; ok {

		return string2LogLevel[level]
	}

	return INFO
}

// -----------------------------------------------------------------------------

func (this *Property) getLoggerLevel(name string) int {

	if level, ok := this.Properties["logger.level."+name]; ok {

		return string2LogLevel[level]
	}

	return this.getRootLevel()
}

// -----------------------------------------------------------------------------
