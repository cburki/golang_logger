//
// Filename     : buffer_writer.go
// Description  : Writer for writing message into a buffer (bytes.Buffer).
// Author       : Christophe Burki
// Maintainer   : Christophe Burki
// Version      : 1.0.0
//

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.

// -----------------------------------------------------------------------------

package logger

// -----------------------------------------------------------------------------

import (
	"bytes"
	"time"
)

type BufferWriter struct {
	buffer         *bytes.Buffer
	logFormat      string
	datetimeFormat string
}

// -----------------------------------------------------------------------------

func NewBufferWriter(buffer *bytes.Buffer) *BufferWriter {

	bw := BufferWriter{
		buffer,
		defaultLogFormat,
		defaultDatetimeFormat,
	}

	return &bw
}

// -----------------------------------------------------------------------------

func (this *BufferWriter) write(msg string, name string, level int, date time.Time, location Location) {

	formatter := NewFormatter(this.logFormat, this.datetimeFormat)
	this.buffer.WriteString(formatter.formatLog(msg, name, level, date, location) + "\n")
}

// -----------------------------------------------------------------------------

func (this *BufferWriter) setLogFormat(format string) {

	this.logFormat = format
}

// -----------------------------------------------------------------------------

func (this *BufferWriter) setDatetimeFormat(format string) {

	this.datetimeFormat = format
}

// -----------------------------------------------------------------------------
